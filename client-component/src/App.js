import React from 'react';
import './App.css';
import { FeedHeader } from './components/FeedHeader';
import FeedList from 'components/FeedList';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { articles: [] };
    this.removeItem = this.removeItem.bind(this);
  }

  componentDidMount() {
    let removed = localStorage.getItem('removed');
    if (removed) {
      removed = JSON.parse(removed);
    }
    fetch('http://localhost:3000/articles')
      .then(response => response.json())
      .then(data => {
        let removed = localStorage.getItem('removed');
        let articles = data;
        if (removed != null) {
          removed = JSON.parse(removed);
          for (let id of removed) {
            articles = articles.filter(el => el._id !== id);
          }
        }
        this.setState({ articles: articles });
      });
  }

  removeItem(id) {
    const articles = this.state.articles.filter(el => el._id !== id);
    this.setState({ articles: articles });
  }

  render() {
    return (
      <div className="App">
        <FeedHeader />
        <FeedList articles={this.state.articles} removeItem={this.removeItem} />
      </div>
    );
  }
}

export default App;
