import React from 'react';

export class FeedHeader extends React.Component {
  render() {
    return (
      <header className="App-header">
        <div className="Header-content">
          <h1>HN Feed</h1>
          <p>We &#60;3 Hacker news</p>
        </div>
      </header>
    );
  }
}
