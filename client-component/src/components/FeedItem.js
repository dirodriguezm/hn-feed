import React from 'react';
import './FeedItem.css';
import moment from 'moment';

export default class FeedItem extends React.Component {
  constructor(props) {
    super(props);
    this.removeItem = this.removeItem.bind(this);
    this.onRowClick = this.onRowClick.bind(this);
    this.state = {
      parsedDate: '',
    };
    // Parse timestamp
    if (moment(props.created_at).isSame(moment(), 'day')) {
      this.state.parsedDate = moment(props.created_at).format('LT');
    } else if (
      moment(props.created_at).isBetween(
        moment()
          .subtract(1, 'days')
          .startOf('day'),
        moment().startOf('day')
      )
    ) {
      this.state.parsedDate = 'Yesterday';
    } else if (moment(props.created_at).isSame(moment(), 'year')) {
      this.state.parsedDate = moment(props.created_at).format('MMM D');
    } else {
      this.state.parsedDate = moment(props.created_at).format('MMM D Y');
    }
  }

  removeItem() {
    let removed = localStorage.getItem('removed');
    if (!removed) {
      removed = [];
    } else {
      removed = JSON.parse(removed);
    }
    const exists = removed.find(el => el === this.props.id);
    if (exists !== -1) {
      removed.push(this.props.id);
      localStorage.setItem('removed', JSON.stringify(removed));
    }
    this.props.removeItem(this.props.id);
  }

  onRowClick() {
    const url = this.props.url || this.props.story_url;
    window.open(url, '_blank').focus();
  }

  render() {
    return (
      <div className="item-container" onClick={this.onRowClick}>
        <div className="title-container">
          <h2 className="item-title">{this.props.title} </h2>
          <p className="item-author">&emsp;-&nbsp;{this.props.author}&nbsp;-</p>
        </div>

        <div className="timestamp-container">
          <h2 className="item-title">{this.state.parsedDate}</h2>
        </div>

        <div className="action-container">
          <button className="remove-button" onClick={this.removeItem}>
            <i className="fa fa-trash"></i>
          </button>
        </div>
      </div>
    );
  }
}
