import React from 'react';
import './FeedList.css';
import FeedItem from 'components/FeedItem';

export default class FeedList extends React.Component {
  render() {
    const validArticles = this.props.articles.filter(
      article => article.title || article.story_title
    );
    return (
      <div className="feed-list-container">
        <div className="feed-list">
          {validArticles.map(article => {
            return (
              <FeedItem
                key={article._id}
                id={article._id}
                title={article.title || article.story_title}
                author={article.author}
                created_at={article.created_at}
                url={article.url || article.story_url}
                removeItem={this.props.removeItem}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
