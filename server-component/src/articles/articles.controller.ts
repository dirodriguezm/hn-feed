import { Controller, Get } from '@nestjs/common';
import { ArticlesService } from './articles.service';
import { ArticleResponseModel } from './interfaces/articles.interface';

@Controller('articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  @Get()
  async getAllArticles(): Promise<ArticleResponseModel[]> {
    return await this.articlesService.getAllArticles();
  }
}
