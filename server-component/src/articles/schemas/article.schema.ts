import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop({ required: true })
  created_at: Date;

  @Prop()
  title: string;

  @Prop()
  story_title: string;

  @Prop()
  story_url: string;

  @Prop()
  url: string;

  @Prop()
  author: string;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
