import { Test } from '@nestjs/testing';
import { ArticlesController } from '../articles.controller';
import { ArticlesService } from '../articles.service';
import { articlesMock } from '../interfaces/articles.mock';
import { ArticlesServiceMock } from './articles.service.mock';

describe('ArticlesController', () => {
  let articlesController: ArticlesController;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [ArticlesController],
      providers: [
        {
          provide: ArticlesService,
          useClass: ArticlesServiceMock,
        },
      ],
    }).compile();

    articlesController = moduleRef.get<ArticlesController>(ArticlesController);
  });

  describe('getAllArticles', () => {
    it('should return an array of articles', async () => {
      const articles = await articlesController.getAllArticles()
      expect(articles).toStrictEqual(
        articlesMock,
      );
    });
  });
});
