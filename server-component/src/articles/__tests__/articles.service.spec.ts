import { MongooseModule } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import {
  closeInMongodConnection,
  rootMongooseTestModule,
} from '../../utils/mongo-test-utils';
import { ArticlesService } from '../articles.service';
import { Article, ArticleSchema } from '../schemas/article.schema';
import mongoose from 'mongoose'

describe('ArticlesService', () => {
  describe('getAllArticles', () => {
    let articlesService: ArticlesService;

    beforeEach(async () => {
      const moduleRef = await Test.createTestingModule({
        imports: [
          rootMongooseTestModule(),
          MongooseModule.forFeature([
            { name: Article.name, schema: ArticleSchema },
          ]),
        ],
        providers: [ArticlesService],
      }).compile();

      articlesService = moduleRef.get<ArticlesService>(
        ArticlesService,
      );
    });

    it('should be defined', () => {
      expect(articlesService).toBeDefined();
    });

    afterAll(async () => {
      await closeInMongodConnection();
    });
  });
});
