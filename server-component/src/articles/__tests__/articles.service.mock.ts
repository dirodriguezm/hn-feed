import { articlesMock } from '../interfaces/articles.mock';
import { Article } from '../schemas/article.schema';

export class ArticlesServiceMock {
  async getAllArticles(): Promise<Article[]> {
    return articlesMock;
  }
}
