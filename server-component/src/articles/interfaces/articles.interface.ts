export interface ArticleResponseModel {
  created_at: Date;
  title: string | null;
  story_title: string | null;
  story_url: string | null;
  url: string | null;
  author: string;
}
