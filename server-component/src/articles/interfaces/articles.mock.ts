import { ArticleResponseModel } from './articles.interface';

export const articlesMock: ArticleResponseModel[] = [
  {
    title: null,
    created_at: new Date('2021-06-26T14:33:30.000Z'),
    story_title: 'The internet eats up less energy than you might think',
    story_url:
      'https://www.nytimes.com/2021/06/24/technology/computer-energy-use-study.html',
    url: null,
    author: 'sneak',
  },
  {
    title: null,
    created_at: new Date('2021-06-26T14:33:30.000Z'),
    story_title: null,
    story_url:
      'https://www.nytimes.com/2021/06/24/technology/computer-energy-use-study.html',
    url: null,
    author: 'sneak',
  },
  {
    title: 'Cloudproxy – hide your scrapers IP behind the cloud',
    created_at: new Date('2021-06-26T14:30:58.000Z'),
    story_title: null,
    story_url: null,
    url: 'https://github.com/claffin/cloudproxy',
    author: 'aeyes',
  },
];
