import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ArticlesModule } from './articles/articles.module';
import { ScheduleModule } from '@nestjs/schedule';
import { MongooseModule } from '@nestjs/mongoose';
import { SchedulerModule } from './db_updater/scheduler/scheduler.module';

@Module({
  imports: [
    ArticlesModule,
    SchedulerModule,
    ScheduleModule.forRoot(),
    MongooseModule.forRoot('mongodb://admin:adminpwd@database:27017/hnfeed'),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
