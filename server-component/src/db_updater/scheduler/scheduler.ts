import { Injectable } from '@nestjs/common';
import { Cron, Timeout } from '@nestjs/schedule';
import { FetchService } from '../fetcher/fetcher.service';

@Injectable()
export class FetchScheduler {
  constructor(private fetchService: FetchService) {}

  fetchFromApi() {
    const articles = this.fetchService.findAll();
    this.fetchService.saveArticles(articles);
  }

  @Timeout(5000)
  initialFetch() {
    this.fetchFromApi();
  }

  @Cron('0 * * * *')
  cronFetch() {
    this.fetchFromApi();
  }
}
