import { Module } from '@nestjs/common';
import { FetcherModule } from '../fetcher/fetcher.module';
import { FetchScheduler } from './scheduler';

@Module({
  imports: [FetcherModule],
  providers: [FetchScheduler],
})
export class SchedulerModule {}
