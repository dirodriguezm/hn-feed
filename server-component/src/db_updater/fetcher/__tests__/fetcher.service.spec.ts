import { HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import {
  Article,
  ArticleSchema,
} from '../../../articles/schemas/article.schema';
import {
  closeInMongodConnection,
  rootMongooseTestModule,
} from '../../../utils/mongo-test-utils';
import { ArticlesModule } from '../../../articles/articles.module';
import { FetchService } from '../fetcher.service';
import { articlesMock } from '../../../articles/interfaces/articles.mock';
import { of } from 'rxjs';

describe('FetchSerevice', () => {
  describe('findAll', () => {
    let fetchService: FetchService;

    beforeEach(async () => {
      const moduleRef = await Test.createTestingModule({
        imports: [
          HttpModule,
          ArticlesModule,
          rootMongooseTestModule(),
          MongooseModule.forFeature([
            { name: Article.name, schema: ArticleSchema },
          ]),
        ],
        providers: [FetchService],
      }).compile();

      fetchService = moduleRef.get<FetchService>(FetchService);
    });

    it('should be defined', () => {
      expect(fetchService).toBeDefined();
    });

    it('should fetch data from remote API', (done) => {
      jest.spyOn(fetchService['httpService'], 'get').mockReturnValue(
        of({
          data: articlesMock,
          status: 200,
          statusText: 'OK',
          headers: {},
          config: {},
        }),
      );
      let data = {};
      fetchService.findAll().subscribe({
        next: (val) => {
          data = val.data;
        },
        error: (err) => {
          throw err;
        },
        complete: () => {
          expect(data).toStrictEqual(articlesMock);
          done();
        },
      });
    });
  });
  afterAll(async () => {
    await closeInMongodConnection();
  });
});
