export interface ArticleList {
  hits: ArticleResponseModel[];
  nbHits: number;
  page: number;
  nbPages: number;
  hitsPerPage: number;
  exhaustiveNbHits: boolean;
  query: string;
  params: string;
  processingTimeMS: number;
}
export interface ArticleResponseModel {
  created_at: Date;
  title: string | null;
  story_title: string | null;
  story_url: string | null;
  url: string | null;
  author: string;
}
