import { HttpModule, Module } from '@nestjs/common';
import { ArticlesModule } from 'src/articles/articles.module';
import { FetchService } from './fetcher.service';

@Module({
  imports: [HttpModule, ArticlesModule],
  providers: [FetchService],
  exports: [FetchService],
})
export class FetcherModule {}
