import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AxiosResponse } from 'axios';
import { Model } from 'mongoose';
import { Observable } from 'rxjs';
import {
  Article,
  ArticleDocument,
} from '../../articles/schemas/article.schema';
import { ArticleList } from './fetcher.interface';

@Injectable()
export class FetchService {
  constructor(
    private httpService: HttpService,
    @InjectModel(Article.name) private articleModel: Model<ArticleDocument>,
  ) {}

  findAll(): Observable<AxiosResponse<ArticleList>> {
    const articles = this.httpService.get(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
    return articles;
  }

  saveArticles(articlesResponse: Observable<AxiosResponse<ArticleList>>): void {
    articlesResponse.subscribe((data) => {
      data.data.hits.forEach((hit) => {
        const createdArticle = new this.articleModel(hit);
        createdArticle.save();
      });
    });
  }
}
