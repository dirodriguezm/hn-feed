# HN Feed

Server and client application to fetch and display articles from HN API using NodeJS + MongoDB + React. The application has two modules, a server component and a client component. Both components and the MongoDB database can be run with docker compose.

To run all the components execute the following command:
```shell
docker-compose up -d
```
This will run everything so you can go to http://localhost:3001 to access the react web app that displays articles. REST API will be available at http://localhost:3000. 

## Server component
NestJS application that fetches data from an external API to populate the MongoDB database with the latest articles. It also exposes a REST API to consume the articles stored in the database.

Articles can be fetched using at the `/articles` (GET) endpoint.

### Considerations
- When starting the server it will populate the empty database with the latest articles
- When fetching data from the external API, it will only use the first page (newer articles)
- The REST API that consumes the MongoDB database is not paginated and will always return the latest 20 articles
- DB connection and external API url are hard coded

**Note**: I had the intention to run tests with a MongoDB in-memory database which is active in tests, but no meaningful test is done using it.

## Client component
React application that displays articles fetched from the Server Component.
### Considerations
- Users can remove items. This config is stored in the browser's local storage.
- Deployment has a build stage that executes the typical react build steps (`npm install` and `npm run build`) followed by a deploy stage which runs nginx with the previously built files.
