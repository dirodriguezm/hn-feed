db.createUser(
  {
    user: "admin",
    pwd: "adminpwd",
    roles: [
      {
        role: "readWrite",
        db: "hnfeed"
      }
    ]
  }
)
